import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import {Request, Response} from 'express';
import {ClientResponse} from "http";
// import {ClientResponse} from '@sendgrid/client/src/response';
// import {ResponseError} from '@sendgrid/helpers/classes';
// import {MailData} from '@sendgrid/helpers/classes/mail';

/**
 *
 * Callback para requisição http para construção de novos usuarios
 *
 * @callback RequestFunction
 * @param req Dados da requisição
 * @param res Resultado da requisição
 */
 export type RequestFunction = (req: Request, res: Response) => void;

/**
 *
 * Callback para requisição http para construção de novos usuarios
 *
 * @callback RequestFunctionAsync
 * @param req Dados da requisição
 * @param res Resultado da requisição
 * @return Retorna uma promise de chamdas assincronas
 */
export type RequestFunctionAsync = (req: Request, res: Response) => Promise<any>;

/**
 *
 * Callback para requisição http para construção de novos usuarios
 *
 * @callback RequestFunction
 * @param req Dados da requisição
 * @param res Resultado da requisição
 * @param next Próxima função de seguimento do middleware
 */
 export type RequestFunctionPromise = (req: Request, res: Response, next: Function) => void;

/**
 *
 * Callback para requisições de baixo nível com erro
 *
 * @callback RequestFunctionPromiseError
 * @param err Tipo de erro apresentado pelo sistema
 * @param req Dados da requição
 * @param res Resultado da requisição
 * @param next Próxima função de seguimento do middleware
 */
 export type RequestFunctionPromiseError = (err: any, req: Request, res: Response, next: Function) => void;

/**
 *
 * Callback para adição de novos usuarios ao banco
 *
 * @callback CallbackNovoUsuarioType
 * @param usuario - Usuario a ser adicionado ao banco de dados.
 * @return Retorna uma promise para dar continuidade ao evento.
 */
export type CallbackUsuarioType = (usuario: admin.auth.UserRecord) => PromiseLike<any> | any;

/**
 *
 * Callback acionado quando algum path receber um novo dado, em algum evento acionador do Realtime Database.
 *
 * @callback CallbackCloudFunctionCreate
 * @param firebase_snapshot Cópia dos dados recebidos após o evento.
 * @param context Contexto da CloudFunction.
 */
 export type CallbackCloudFunctionCreate = (firebase_snapshot: functions.database.DataSnapshot, context: functions.EventContext) => PromiseLike<any> | any;

/**
 *
 * Callback acionado quando algum path remover um dado, em algum evento acionador do Realtime Database.
 *
 * @callback CallbackCloudFunctionCreate
 * @param firebase_snapshot Cópia dos dados recebidos após o evento.
 * @param context Contexto da CloudFunction.
 */
export type CallbackCloudFunctionDelete = CallbackCloudFunctionCreate;

/**
 *
 * Callback acionado quando algum path atualizar algum dado seja ele qual for, em algum evento acionador do Realtime Database.
 *
 * @callback CallbackCloudFunctionUpdate
 * @param firebase_change_snapshot Cópia dos dados antes e depois do evento.
 * @param context Contexto da CloudFunction.
 */
export type CallbackCloudFunctionUpdate = (firebase_change_snapshot: functions.Change<functions.database.DataSnapshot>, context: functions.EventContext) => PromiseLike<any> | any;

/**
 *
 * Callback acionado para funções agendadas
 *
 * @callback CallbackScheduleFunction
 * @param context Contexto da CloudFunction.
 */
export type CallbackScheduleFunction = (context: functions.EventContext) => PromiseLike<any> | any;

