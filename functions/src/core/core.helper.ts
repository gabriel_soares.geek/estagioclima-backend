import * as path from "path";
import * as fs from "fs";
import {tmpdir} from "os";
import * as admin from "firebase-admin";

/**
 *
 * Executa uma promise com async await de forma sincrona
 *
 * @param fn Função a ser executada
 */
export const asyncMiddleware = (fn) => {
    return (req, res, next) => {
        Promise.resolve(fn(req, res, next))
            .catch((err) => {
                next(err);
            });
    };
};

/**
 * Converte segundos para milisegundos
 *
 * @param segundos Quantidade de segundos a ser convertida
 */
export const segundosToMs = (segundos : number) => (segundos * 1000);

/**
 * Converte a quantidade de minutos para milisegundos
 *
 * @param minutos Quantidade em minutos a ser convertitda
 */
export const minutosToMs = (minutos : number) => (minutos * segundosToMs(60));

/**
 * (c) 2012 Steven Levithan <http://slevithan.com/>
 * MIT license
 */
if (!String.prototype.codePointAt) {
    String.prototype.codePointAt = function (pos) {
        const pos1 = isNaN(pos) ? 0 : pos;
        const str = String(pos1),
            code = str.charCodeAt(pos1),
            next = str.charCodeAt(pos1 + 1);
        // If a surrogate pair
        if (0xD800 <= code && code <= 0xDBFF && 0xDC00 <= next && next <= 0xDFFF) {
            return ((code - 0xD800) * 0x400) + (next - 0xDC00) + 0x10000;
        }
        return code;
    };
}

/**
 * Encodes special html characters DEPRECATED, use LODASH
 * @param string
 * @return {*}
 */
export function html_encode(string) {
    let ret_val = '';
    for (let i = 0; i < string.length; i++) {
        if (string.codePointAt(i) > 127) {
            ret_val += '&#' + string.codePointAt(i) + ';';
        } else {
            ret_val += string.charAt(i);
        }
    }
    return ret_val;
}

/**
 *
 * Função auxiliar da função que insere imagens no Firebase Storage
 *
 * @param targetDir
 * @param isRelativeToScript
 */
export const mkDirByPathSync = (targetDir:string, { isRelativeToScript = false } = {}) => {
    const sep = path.sep;
    const initDir = path.isAbsolute(targetDir) ? sep : '';
    const baseDir = isRelativeToScript ? __dirname : '.';

    return targetDir.split(sep).reduce((parentDir, childDir) => {
        const curDir = path.resolve(baseDir, parentDir, childDir);
        try {
            fs.mkdirSync(curDir);
        } catch (err) {
            if (err.code === 'EEXIST') { // curDir already exists!
                return curDir;
            }

            // To avoid `EISDIR` error on Mac and `EACCES`-->`ENOENT` and `EPERM` on Windows.
            if (err.code === 'ENOENT') { // Throw the original parentDir error on curDir `ENOENT` failure.
                throw new Error(`EACCES: permission denied, mkdir '${parentDir}'`);
            }

            const caughtErr = ['EACCES', 'EPERM', 'EISDIR'].indexOf(err.code) > -1;
            if (!caughtErr || caughtErr && curDir === path.resolve(targetDir)) {
                throw err; // Throw if it's just the last created dir.
            }
        }

        return curDir;
    }, initDir);
};

/**
 * Função utilizada no Back-End para inserir imagens no Firebase Storage
 */
export const inserirImagemStorage = async (
    caminho_vetor: string[],
    img_base64: string,
    nome: string): Promise<object> => {

    // Caminho base
    let path_default = '.';

    // Cada diretório recebido como paramêtro é adicionado ao path base
    for (const diretorio in caminho_vetor) {
        path_default = path.join(path_default, caminho_vetor[diretorio]);
    }

    if (!img_base64.startsWith('data:image/')) {
        return {
            dados: null,
            status: false
        };
    }

    try {

        // Local do arquivo
        const fotoPath = path.join(path_default, (nome + '.jpg'));
        const tmpFotoPath = path.join(tmpdir(), fotoPath);
        const tmpFotoSavePathDir = path.join(tmpdir(), path_default);

        // Cria pasta para arquivo temporário
        if (!fs.existsSync(tmpFotoSavePathDir)) {
            mkDirByPathSync(tmpFotoSavePathDir);
        }

        // Strip off the data: url prefix to get just the base64-encoded bytes
        const base64img = img_base64.replace(/^data:image\/\w+;base64,/, '');
        const buf = new Buffer(base64img, 'base64');

        // Grava imagem no disco
        fs.writeFileSync(tmpFotoPath, buf);

        // Envio e espero pelo fim da execução daquela foto!
        await admin
            .storage()
            .bucket()
            .upload(tmpFotoPath, {
                // Support for HTTP requests made with `Accept-Encoding: gzip`
                destination: fotoPath,
                gzip: true,
                metadata: {
                    // Enable long-lived HTTP caching headers
                    // Use only if the contents of the file will never change
                    // (If the contents will change, use cacheControl: 'no-cache')
                    cacheControl: 'public, max-age=31536000'
                }
            });

        // Obtendo dados da imagem enviada
        const myFile = admin.storage().bucket().file(fotoPath);

        // Vetor de urls
        const resultado = await myFile.getSignedUrl({action: 'read', expires: '03-09-2491'});

        // Objetos de informações da foto
        const img_info = {
            download_url: resultado[0],
            caminho_url: fotoPath,
            nome: nome,
            tipo: 'img/jpeg'
        };

        return {
            dados: img_info,
            status: true
        };

    }
    catch(erro) {
        return {
            dados: null,
            status: false
        };
    }

};
