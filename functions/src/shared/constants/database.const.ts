/**
 * Constantes utilizadas para paths comuns no firebase.
 */
export class DatabasePaths {

    /**
     * Path para usuarios administradores.
     */
    public static readonly ADMINISTRADORES = '/administradores';
    public static readonly ADMINISTRADORES_ID = `${DatabasePaths.ADMINISTRADORES}/{administradores_id}`;

    /**
     * Path para usuarios administradores.
     */
    public static readonly USUARIOS = '/usuarios';
    public static readonly USUARIOS_ID = `${DatabasePaths.USUARIOS}/{usuarios_id}`;

    /**
     * Path para usuarios administradores.
     */
    public static readonly DINHEIRO = '/dinheiro';
    public static readonly DINHEIRO_USUARIOS_ID = `${DatabasePaths.USUARIOS}/{usuarios_id}`;

    /**
     * Path Hello World
     */
    public static readonly HELLO_WORLD = '/hello_world/hello';

}
