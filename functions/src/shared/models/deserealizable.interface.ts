/**
 * Implementa uma interface de deserealização para modelos.
 */
export interface Deserializable {

    /**
     *
     * Deserealiza um objeto recebido por parâmetro, seja por uma requisição ou conjunto de dados.
     *
     * @param tipo Tipo da deserealização
     */
    deserialize(tipo: number): object;

}