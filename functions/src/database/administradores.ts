import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import * as lodash from 'lodash';
import {
	CallbackCloudFunctionCreate
} from '../core/interfaces.type';

export const onCreateAdminCaixaAlta = (
	database: admin.database.Database
) : CallbackCloudFunctionCreate => {
	return async (snapshot: functions.database.DataSnapshot, context: functions.EventContext): Promise<any> => {
        try {
            const user = context.params.administradores_id;
            const dado = snapshot.val();
            const dado_caixa_alta = lodash.upperCase(dado.estado);
            await database.ref('administradores').child(user).update({'estado': dado_caixa_alta});
            await admin.firestore().collection('/administradores').doc(user).update({estado: dado_caixa_alta});
            // console.log('[INFO] {callbackHelloWorld} - Hello world caixa alta com sucesso');
    }
    catch (erro){
        console.error(new Error(erro.message));
    }  
    };
};