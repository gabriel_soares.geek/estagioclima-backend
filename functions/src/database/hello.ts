import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import * as lodash from 'lodash';
import {
	CallbackCloudFunctionCreate
} from '../core/interfaces.type';

export const callbackHelloCaixaAlta = (
	database: admin.database.Database
) : CallbackCloudFunctionCreate => {
	return async (snapshot: functions.database.DataSnapshot, context: functions.EventContext): Promise<any> => {

		try {
                const dado = snapshot.val();
                const dado_caixa_alta = lodash.upperCase(dado);
                await database.ref('hello_world').child('hello').set(dado_caixa_alta);
                console.log('[INFO] {callbackHelloWorld} - Hello world caixa alta com sucesso');
        }
        catch (erro){
            console.error(new Error('[ERRO] {callbackHelloWorld} - Problemas com o Hello World caixa alta: ' + erro.message));
        }   
        
    }
};
