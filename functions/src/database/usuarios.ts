import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import * as lodash from 'lodash';
import { DatabasePaths } from '../shared/constants/database.const';
import {
    CallbackCloudFunctionCreate
} from '../core/interfaces.type';

export const changeStateFromUser = (
    database: admin.database.Database
) : CallbackCloudFunctionCreate => {
    return async (snapshot: functions.database.DataSnapshot, context: functions.EventContext): Promise<any> => {

        try {
            const dado = snapshot.val();
            const id = context.params.usuarios_id;
            const dado_caixa_alta = lodash.upperCase(dado.estado);
            console.log(dado);
            console.log(id);
            console.log(dado_caixa_alta);
            await database.ref('usuarios').child(id).child('estado').set(dado_caixa_alta);
        }
        catch (erro){
            console.error(new Error('[ERRO] {changeStateFromAdmin} - Problemas com o Estado caixa alta: ' + erro.message));
        }

    }
};
