import {EnviromentModel} from './environment';

/**
 * Variavéis de ambiente para produção.
 */
export const environment: EnviromentModel = {
	firebase: {
		cert_file: 'serviceAccountKey.prod',
		server_uid: 'COP1AN40com3d1a',
		database_url: 'https://estagio-clima.firebaseio.com/',
		database_instance: 'estagio-clima',
		storage_bucket: 'estagio-clima.appspot.com/'
	},
	cors: {
		origens: [
		]
	}
};
