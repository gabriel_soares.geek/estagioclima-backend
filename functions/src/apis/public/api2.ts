import * as admin from "firebase-admin";
import { RequestFunctionAsync } from "../../core/interfaces.type";
import { Request, Response } from "express";
const axios = require('axios').default;

const instance = axios.create();


export const readAll = (
    database: admin.database.Database
): RequestFunctionAsync => {
    return async (req: Request, res: Response): Promise<any> => {
        try {

            const usuarios = (await database.ref('usuarios').once('value')).val();

            res.status(200).json(usuarios);
        }catch (err) {
            console.error(new Error(err));
            res.status(500).json({
                message: 'não rolou'
            })
        };
    };
};

export const read = (
    database: admin.database.Database
): RequestFunctionAsync => {
    return async (req: Request, res: Response): Promise<any> => {
        try {
            const user = req.params.user;
            const usuarios = (await database.ref('usuarios')
                .child(user)
                .once('value')).val();

            res.status(200).json(usuarios);
        }catch (err) {
            console.error(new Error(err));
            res.status(500).json({
                message: 'não rolou'
            })
        };
    };
};

export const write = (
    database: admin.database.Database
): RequestFunctionAsync => {
    return async (req: Request, res: Response): Promise<any> => {
        try {
            const {user_name, user_email,user_city, user_state} = req.body;
            const usuario = await database.ref('usuarios').push({
                'nome': user_name,
                'email': user_email,
                'cidade': user_city,
                'estado': user_state
            });

            res.status(200).json({
                message: 'ok, tudo massa',
                chave_user: usuario.key
            });

        }catch (err) {
            console.error(new Error(err));
            res.status(500).json({
                message: 'não rolou'
            })
        };
    };
};

export const del = (
    database: admin.database.Database
): RequestFunctionAsync => {
    return async (req: Request, res: Response): Promise<any> => {
        try {
            req.body.user_id;
            await database.ref('usuarios')
                .child(req.body.user_id)
                .set(null);

            res.status(200).json({
                message: 'ok, apaguei'
            });

        }catch (err) {
            console.error(new Error(err));
            res.status(500).json({
                message: 'não rolou'
            })
        };
    };
};

export const update = (
    database: admin.database.Database
): RequestFunctionAsync => {
    return async (req: Request, res: Response): Promise<any> => {
        try {
            const user = req.params.user;
            const {user_name, user_email,user_city, user_state} = req.body;
            await database.ref('usuarios').child(user).update({
                'nome': user_name,
                'email': user_email,
                'cidade': user_city,
                'estado': user_state
            });

            res.status(200).json({
                message: 'ok, alterado'
            });

        }catch (err) {
            console.error(new Error(err));
            res.status(500).json({
                message: 'não rolou'
            })
        };
    };
};

export const predictWeather = (
    database: admin.database.Database
): RequestFunctionAsync => {
    return async(req: Request, res: Response): Promise<any> => {
        const data = axios.get('https://api.hgbrasil.com/weather?array_limit=2&fields=only_results,temp,city_name,forecast,max,min,date&city_name=Salvador,BA&key=4f4a7c8d')
        .then (async function (response) {
            console.log(response.data);
            const {user_id} = req.body;
            // const previsoes = database.ref('previsoes');
            try {
                const usuario = (await database.ref('/previsoes')
                .child(user_id)
                .once('value')).val()
                if(usuario === null){
                    console.log(usuario);
                    await database.ref('/previsoes').child(user_id).update({
                        'numero_consultas': 1
                    });
                }else{
                    await database.ref('/previsoes/' + user_id + '/numero_consultas').transaction(function(consulta){
                        if(consulta >= 0){
                            return consulta + 1;
                        }
                    });
                }

                await database.ref('/previsoes').child(user_id).update({
                    'timestamp': Date.now(),
                    'data': response.data.date,
                    'dados': response.data.forecast
                });

                res.status(200).json({
                    message: 'ok, tudo massa'
                });

            }catch (err) {
                console.error(new Error(err));
                res.status(500).json({
                    message: 'n',
                    dado: response.data
                })
            };
        })
        .catch (function(error) {
            res.status(500).json({
                error: error.message,
                message: 'não pegou nada da api'
            });
        });
    }

}