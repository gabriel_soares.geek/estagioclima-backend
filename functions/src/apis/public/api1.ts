import * as admin from "firebase-admin";
import axios from "axios";
import * as moment from "moment-timezone";
import { RequestFunctionAsync } from "../../core/interfaces.type";
import { Request, Response } from "express";
import { firebaseConfig } from "firebase-functions";

const problem = (err, res) => {
    console.error(new Error('[ERRO] - Problemas com esta rota: ' + err.message));
    res.status(500).json({
        message: '[ERRO] - Problemas com esta rota: ' + err.message,
        status: false
    })
}

export const getMoney = (database: admin.database.Database): RequestFunctionAsync => {
    return async (req: Request, res) => {
        const user_id = req.params.id;
        const fields = "only_results,currencies";
        const key = "4f4a7c8d";
        axios.get(`http://api.hgbrasil.com/finance/quotations?&fields=${fields}&key=${key}`)
            .then(async function (response) {

                const oneuser = (await database.ref('usuarios').child(user_id).once('value')).val();
                const maxconsultas = (await database.ref('dinheiro').child(user_id).child('numero_consultas').once('value')).val();
                const databefore = (await database.ref('dinheiro').child(user_id).child('data').once('value')).val();

                const moedas = {
                    USD: {
                        nome: response.data.currencies.USD.name,
                        valor: response.data.currencies.USD.buy
                    },
                    EUR: {
                        nome: response.data.currencies.EUR.name,
                        valor: response.data.currencies.EUR.buy
                    },
                    BTC: {
                        nome: response.data.currencies.BTC.name,
                        valor: response.data.currencies.BTC.buy
                    }
                };

                const data = () => {
                    return moment().tz("America/Bahia").format("DD/MM/YYYY");
                }

                const timestamp = () => {
                    return moment().tz("America/Bahia").valueOf();
                }

                const consulta = {
                    "timestamp": timestamp(),
                    "data": data(),
                    "moedas": moedas
                };

                if (databefore === consulta.data) {
                    if (maxconsultas < 10) {
                        await database.ref('dinheiro').child(user_id).child('numero_consultas').transaction(function (searches) {
                            return (searches || 0) + 1
                        });
                    } else {
                        res.status(429).json({ message: "Número máximo de consultas atingido", status: true });
                    }
                } else {
                    await database.ref('dinheiro').child(user_id).child('numero_consultas').transaction(function (searches) {
                        return 1
                    });
                }

                if (oneuser) {
                    console.log(maxconsultas);
                    await database.ref('dinheiro').child(user_id).update(consulta);
                    res.status(200).json({ message: "Realizou a consulta", moedas: moedas, status: true });
                } else {
                    console.log(maxconsultas);
                    res.status(404).json({ message: "Não encontrou usuario pra fazer consulta", status: true });
                }
            })
            .catch(function (err) {
                problem(err, res);
            })
    };
};

export const showAdmin = (database: admin.database.Database): RequestFunctionAsync => {
    return async (req: Request, res: Response): Promise<any> => {
        try {
            const alladmin = (await database.ref('administradores').once('value')).val();
            const alladmin_firestore = (await admin.firestore().collection('administradores').get());

            const dados = {};
            alladmin_firestore.forEach(snapshot => {
                dados[snapshot.id] = snapshot.data()
            })

            res.status(200).json({ database: alladmin, firestore: dados });
        }
        catch (err) {
            problem(err, res);
        }
    };
};

export const showOneAdmin = (database: admin.database.Database): RequestFunctionAsync => {
    return async (req: Request, res: Response): Promise<any> => {
        try {
            const id = req.params.id;

            const oneadmin = (await database.ref('administradores').child(id).once('value')).val();
            const alladmin_firestore = (await admin.firestore().collection('administradores').get());

            const dados = {};
            alladmin_firestore.forEach(snapshot => {
                if (snapshot.id === id) {
                    dados[snapshot.id] = snapshot.data()
                }
            })

            if (oneadmin && dados) {
                res.status(200).json({
                    message: 'Encontrou administrador',
                    admin: oneadmin,
                    admin_firestore: dados,
                    status: true
                });
            } else {
                res.status(404).json({
                    message: 'Administrador não encontrado',
                    status: true
                });
            }
        }
        catch (err) {
            problem(err, res);
        }
    };
};

export const showOneAdminWithQuery = (database: admin.database.Database): RequestFunctionAsync => {
    return async (req: Request, res: Response): Promise<any> => {
        try {
            const { nome, email, cidade, estado } = req.query;

            // tslint:disable-next-line: prefer-const
            const adm = admin.firestore().collection('administradores');
            let collectionRef;
            collectionRef = adm;
            if (nome) {
                collectionRef = collectionRef.where('nome', '==', nome);
            } if (email) {
                collectionRef = collectionRef.where('email', '==', email);
            } if (cidade) {
                collectionRef = collectionRef.where('cidade', '==', cidade);
            } if (estado) {
                collectionRef = collectionRef.where('estado', '==', estado);
            }

            collectionRef.get()
                .then(querySnapshot => {
                    const dados = {};
                    querySnapshot.forEach(documentSnapshot => {
                        console.log(`Found document at ${documentSnapshot.ref.path}`);
                        dados[documentSnapshot.id] = documentSnapshot.data()
                    });
                    res.status(200).json({
                        message: 'Admins encontrados',
                        admin_query: dados,
                        status: true
                    });
                }).catch(err => problem(err, res));
        }
        catch (err) {
            problem(err, res);
        }
    };
};

export const createAdmin = (database: admin.database.Database): RequestFunctionAsync => {
    return async (req: Request, res) => {
        try {
            console.log('1')
            const admin_id = req.body.idadmin;
            admin.auth().getUser(admin_id)
                .then(async function (adminRecord) {
                    console.log('2')
                    if (adminRecord.customClaims['role'] === 0) {
                        console.log('3')
                        const { nome, email, password, status, cidade, estado, telefone } = req.body;
                        let adm;
                        if (nome && email && password && !(status === undefined) && cidade && estado && telefone) {
                            console.log('4')
                            adm = {
                                "nome": nome,
                                "email": email,
                                "status": status,
                                "cidade": cidade,
                                "estado": estado
                            }
                        }
                        if (adm) {
                            console.log('5')
                            const emailregex = email.split(/(\w+[^]+\w)+([.])/);
                            const dotemailregex = emailregex[emailregex.length - 1];
                            const phoneregex = telefone.match(/(\+[0-9]{13}$)/);

                            function confirm() {
                                if((dotemailregex === 'com' || dotemailregex === 'br') && (phoneregex !== null)) {
                                    return true;
                                } else {
                                    return false;
                                }
                            }

                            if (confirm()) {
                                console.log('6')
                                let finduseremail: boolean;
                                let finduserphone: boolean;

                                await admin.auth().getUserByEmail(email)
                                    .then(function () {
                                        finduseremail = true;
                                    })
                                    .catch(function () {
                                        finduseremail = false;
                                    })

                                await admin.auth().getUserByPhoneNumber(telefone)
                                    .then(function () {
                                        finduserphone = true;
                                    })
                                    .catch(function () {
                                        finduserphone = false;
                                    })

                                if (finduseremail || finduserphone) {
                                    res.json({ message: "Problema" });
                                    console.log('tem email ou telefone igual');
                                    return;
                                }
                                console.log('7')
                                admin.auth().createUser({
                                    displayName: nome,
                                    email: email,
                                    password: password,
                                    disabled: !status,
                                    phoneNumber: telefone
                                }).then(async function (userRecord) {
                                    admin.auth().setCustomUserClaims(userRecord.uid, { role: 0 }).then(async () => {
                                        console.log(userRecord.uid);

                                        await database.ref('administradores').child(userRecord.uid).set(adm);
                                        await admin.firestore().collection('administradores').doc(userRecord.uid).set(adm);
                                        console.log('8')
                                        res.status(200).json({
                                            message: "Criou um novo administrador",
                                            chave_admin: userRecord.uid, admin: adm, status: true
                                        });
                                        return;

                                    }).catch(function (error) {
                                        res.json({ message: "Problema ao definir privilégio!", error: error, status: true });
                                    });
                                })
                                    .catch(function (error) {
                                        res.json({ message: "Erro ao criar novo usuário", error: error, status: true });
                                    });
                            } else {
                                console.log('res 1');
                                res.status(406).json({ message: "Email ou telefone inválidos", status: true });
                                return;
                            }
                        } else {
                            console.log('res 2');
                            res.json({ message: "Coloque todos os campos!", status: true });
                            return;
                        }
                    } else {
                        console.log('res 3');
                        res.status(401).json({ message: "Não autorizado!", status: true });
                        return;
                    }
                })
                .catch(function (error) {
                    console.log('res 4');
                    res.status(404).json({ message: "Admin não encontrado!", status: true });
                    return;
                });
        }
        catch (err) {
            problem(err, res);
        }
    };
};

export const updateAdmin = (database: admin.database.Database): RequestFunctionAsync => {   
    return async (req: Request, res) => {
        try {
            const admin_id = req.body.idadmin;
            admin.auth().getUser(admin_id)
                .then(async function (adminRecord) {
                    if (adminRecord.customClaims['role'] === 0) {
                        const id = req.params.id;
                        const { nome, email, password, status, cidade, estado, telefone } = req.body;
                        const adm = {
                            "nome": nome,
                            "email": email,
                            "status": status,
                            "cidade": cidade,
                            "estado": estado
                        };
                        if (adm) {
                            const emailregex = email.split(/(\w+[^]+\w)+([.])/);
                            const dotemailregex = emailregex[emailregex.length - 1];
                            if (dotemailregex === 'com' || dotemailregex === 'br') {

                                admin.auth().updateUser(id, {
                                    displayName: nome,
                                    email: email,
                                    password: password,
                                    disabled: !status,
                                    phoneNumber: telefone
                                }).then(async function () {
                                    await database.ref('/administradores/' + id).update(adm);
                                    await admin.firestore().collection('/administradores/').doc(id).update(adm);

                                    res.status(200).json({
                                        message: "Atualizou administrador",
                                        chave_admin: id, admin: adm, status: true
                                    });
                                }).catch(function (error) {
                                    res.status(404).json({ message: "Erro ao encontrar admin para update", error: error, status: true });
                                });

                            } else {
                                res.status(406).json({ message: "Email inválido", status: true });
                            }
                        } else {
                            res.json({ message: "Coloque todos os campos!", status: true });
                        }
                    } else {
                        res.status(401).json({ message: "Não autorizado!", status: true });
                    }
                })
                .catch(function (error) {
                    res.status(404).json({ message: "Admin não encontrado!", status: true });
                });
        }
        catch (err) {
            problem(err, res);
        }
    };
};

export const deleteAdmin = (database: admin.database.Database): RequestFunctionAsync => {
    return async (req: Request, res) => {
        try {
            const admin_id = req.body.idadmin;
            admin.auth().getUser(admin_id)
                .then(async function (adminRecord) {
                    if (adminRecord.customClaims['role'] === 0) {
                        const id = req.params.id;
                        admin.auth().deleteUser(id).then(async function () {
                            await database.ref('/administradores/' + id).set({
                                "nome": null,
                                "email": null,
                                "status": null,
                                "cidade": null,
                                "estado": null
                            });
                            await admin.firestore().collection('/administradores').doc(id).delete();
                            res.status(200).json({ message: "Deletou admin administrador", status: true });
                        }).catch(function (error) {
                            res.json({ message: "Erro ao atualizar usuário", error: error, status: true });
                        });
                    } else {
                        res.status(401).json({ message: "Não autorizado!", status: true });
                    }
                })
                .catch(function (error) {
                    res.status(404).json({ message: "Admin não encontrado!", status: true });
                });
        }
        catch (err) {
            problem(err, res);
        }
    };
};